var Table = require('reactabular').Table;
var Search = require('reactabular').Search;
var Paginator = require('react-pagify');

var columns = [{
    property: 'team1',
    header: 'Drużyna 1'
  }, {
    property: 'team2',
    header: 'Drużyna 2'
  }, {
    property: 'result',
    header: 'Wynik'
  }
]

module.exports = React.createClass({

  getInitialState: function() {
    return {
      data: [],
      search: {
          column: '',
          query: ''
      },
      pagination: {
          page: 0,
          perPage: 15
      }
    }
  },

  onSearch: function(search) {
    var data = this.getData();

    if (search.query) {
        // apply search to data
        // alternatively you could hit backend `onChange`
        // or push this part elsewhere depending on your needs
        data = Search.search(
            data,
            columns,
            this.state.search.column,
            this.state.search.query
        );
    }

    var pagination = this.state.pagination;
    pagination.page = 0;

    this.setState({
      data: data,
      search: search, // needed?
      pagination: pagination
    });
  },

  getData: function() {
    var data = this.original_data;

    return data;
  },

  onPageSelect: function(page) {
    var pagination = this.state.pagination || {};

    pagination.page = page;

    this.setState({
        pagination: pagination
    });
  },

  onPerPage: function(e) {
    var pagination = this.state.pagination || {};

    pagination.perPage = parseInt(event.target.value, 10);

    this.setState({
        pagination: pagination
    });
  },

  componentDidMount: function() {
    var url = '/games.json';
    //var url = 'http://www.pfkc.pl/includes/plc_test/json.php?type=results&view=list&id=c4ca4238a0-92c7e72833-0ff89b2195841becaf5f56f8f03ce24b';
    $.ajax({
      url: url,
      dataType: 'JSON'
    }).always(function(result) {
      var data = result;
      if (this.isMounted()) {
        this.original_data = data;
        this.setState({
          data: data
        });
      }
    }.bind(this));
  },

  render: function() {
    var paginated = Paginator.paginate(this.state.data, this.state.pagination);
    return (
      <div className='gamesTable'>
        <div className='search-container'>
          <Search columns={columns} data={this.state.data} onChange={this.onSearch}></Search>
        </div>
        <Table columns={columns} data={paginated.data} />
        <div className='pagination'>
          <Paginator
            page={paginated.page}
            pages={paginated.amount}
            beginPages={3}
            endPages={3}
            onSelect={this.onPageSelect}></Paginator>
        </div>
      </div>
    );
  }
});
