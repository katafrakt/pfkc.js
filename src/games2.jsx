module.exports = React.createClass({

  getInitialState: function() {
    return {
      data: [],
      search_query: '',
      league_filter: 'both',
      round_no: 0,
      reverse_order: false,
      pagination: {
          page: 0,
          perPage: 15
      },
      game_status: {
        planned: true,
        ongoing: true,
        completed: true
      }
    }
  },

  onLeagueSelect: function(event) {
    this.setState({
      league_filter: event.target.value
    }, this.updateTable);
  },

  onSetRoundNo: function(event) {
    this.setState({
      round_no: event.target.value
    }, this.updateTable);
  },

  onSearch: function(event) {
    this.setState({
      search_query: event.target.value.toLowerCase()
    }, this.updateTable);
  },

  onStatusSelect: function() {
    var new_status = {};
    $('input[data-role=statusFilter]').each(function(_idx, item) {
      item = $(item);
      new_status[item.data('for')] = item.is(':checked');
    });
    
    this.setState({
      game_status: new_status
    }, this.updateTable);
  },

  onReverseOrder: function(event) {
    this.setState({
      reverse_order: event.target.checked
    }, this.updateTable);
  },

  updateTable: function() {
    var data = this.getData();
    var query = this.state.search_query;

    data = _.filter(data, function(item) {
      return item.team1.toLowerCase().indexOf(query) > -1 || item.team2.toLowerCase().indexOf(query) > -1;
    });

    if(this.state.reverse_order)
      data = data.reverse();

    this.setState({
      data: data
    });
  },

  filterLeague: function(data) {
    var filter = this.state.league_filter;
    if (filter == 'both') return data;

    if (filter == 'male') {
      return _.filter(data, function(item) {
        return item.groupnumber == 'Liga M';
    })} else {
      return _.filter(data, function(item) {
        return item.groupnumber == 'Liga K';
    })}
  },

  filterRound: function(data) {
    var round = this.state.round_no;
    if (round == 0) return data;

    return _.filter(data, function(item) {
      return item.roundnumber == round;
    });
  },

  filterStatus: function(data) {
    var status_map = {
      Z: 'completed',
      P: 'ongoing',
      N: 'planned'
    };
    var self = this;

    return _.filter(data, function(item) {
      var status = status_map[item.status];
      return self.state.game_status[status];
    })
  },

  getData: function() {
    var data = this.original_data;
    data = this.filterLeague(data);
    data = this.filterRound(data);
    data = this.filterStatus(data);
    return data;
  },

  maxRoundNo: function() {
    return _.max(this.original_data, 'roundnumber').roundnumber;
  },

  componentDidMount: function() {
    var url = '/games.json';
    //var url = 'http://www.pfkc.pl/includes/plc_test/json.php?type=results&view=list&id=c4ca4238a0-92c7e72833-0ff89b2195841becaf5f56f8f03ce24b';
    $.ajax({
      url: url,
      dataType: 'JSON'
    }).always(function(result) {
      var data = result;
      if (this.isMounted()) {
        this.original_data = data;
        this.setState({
          data: data
        });
        $('.games-table table').stickyTableHeaders();
      }
    }.bind(this));
  },

  render: function() {
    var self = this;
    var rows = this.state.data.map(function(row) {
      return (
        <TableRow data={row} query={self.state.search_query}></TableRow>
      )
    });
    return (
      <div className="games-table">
        <form className="pure-form filter-form">
          <fieldset>
            <div className="pure-g">
              <div className="pure-u-1 pure-u-md-1-4">
                <input type="text" onChange={this.onSearch} placeholder="Filtruj drużyny" />
              </div>
              <div className="pure-u-1 pure-u-md-1-4">
                <select onChange={this.onLeagueSelect}>
                  <option value="both">wszystkie</option>
                  <option value="male">liga męska</option>
                  <option value="female">liga kobieca</option>
                </select>
              </div>
              <div className="pure-u-1 pure-u-md-1-4">
                <input type="number" min="0" max={this.maxRoundNo()} onChange={this.onSetRoundNo} placeholder="Kolejka" />
              </div>
              <div className="pure-u-1 pure-u-md-1-4">
                <label className="pure-checkbox">
                  <input type="checkbox" data-role="statusFilter" data-for="planned" checked={this.state.game_status.planned} onChange={this.onStatusSelect} /> Nierozpoczęte
                </label>
                <label className="pure-checkbox">
                  <input type="checkbox" data-role="statusFilter" data-for="ongoing" checked={this.state.game_status.ongoing} onChange={this.onStatusSelect} /> Trwające
                </label>
                <label className="pure-checkbox">
                  <input type="checkbox" data-role="statusFilter" data-for="completed" checked={this.state.game_status.completed} onChange={this.onStatusSelect} /> Zakończone
								</label>
              </div>
              <div className="pure-u-1 pure-u-md-1-4">
                <label for="reverseOrder" className="pure-checkbox">
                  <input id="reverseOrder" type="checkbox" onChange={this.onReverseOrder} /> 
                  Odwróć kolejność
                </label>
              </div>
            </div>
          </fieldset>
        </form>
        <table className="pure-table pure-table-horizontal">
          <thead>
            <tr>
              <th><abbr title="Numer kolejki">#K</abbr></th>
              <th>Drużyna 1</th>
              <th>Drużyna 2</th>
              <th>Wynik</th>
              <th><abbr title="Status">S</abbr></th>
            </tr>
          </thead>
          <tbody>
            {rows}
          </tbody>
        </table>
      </div>
    )
  }
});

var TableRow = React.createClass({
  winner: function() {
    var results = this.props.data.result.split(':').map(function(i) { return parseInt(i)});
    if (results[0] > results[1]) {
      return 'team1';
    } else if (results[0] < results[1]) {
      return 'team2';
    } else {
      return 'tie';
    }
  },

  render: function() {
    var cx = React.addons.classSet;
    var team1_classes = cx({
      team: true,
      winner: this.winner() == 'team1'
    });
    var team2_classes = cx({
      team: true,
      winner: this.winner() == 'team2'
    });

    return (
      <tr>
        <td>{this.props.data.roundnumber}</td>
        <td className={team1_classes}>{this.props.data.team1}</td>
        <td className={team2_classes}>{this.props.data.team2}</td>
        <td>{this.props.data.result}</td>
        <td>{this.props.data.status}</td>
      </tr>
    )
  }
})